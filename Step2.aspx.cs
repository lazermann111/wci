﻿using WineCellarPayment.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WineCellarPayment
{
    public partial class Step2 : System.Web.UI.Page
    {
        public static OrderInfo ShipToInfo = new OrderInfo();
        public string orderIdS;
        public string orderRef;
        String token = String.Empty;
        
        protected void NextButton_OnServerClick(object sender, ImageClickEventArgs e)
        {
            if (AgreeToTerms.Checked)
                Response.Redirect("Step3.aspx");

            else
                ValidationMessage.Visible = true;
        }
        protected void PrevButton_OnServerClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Step1.aspx");
        } 
    }
}