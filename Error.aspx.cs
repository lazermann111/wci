﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WineCellarPayment.Model;

namespace WineCellarPayment
{
    public partial class Error : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = ((WineOrder) Session["order"]).OrderMessage;
        }
    }
}
