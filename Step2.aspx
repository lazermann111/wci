﻿<%@ Page Language="C#" MasterPageFile="~/StepsPages.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Step2.aspx.cs" Inherits="WineCellarPayment.Step2" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.HtmlControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Import Namespace="CoffeeShopWebApp" %>


<asp:Content ContentPlaceHolderID="TitlePlaceHolder" runat="server">
    
    <title>
        WCI - Payment Portal
    </title>
    <link rel="stylesheet" type="text/css" href="./Content/css">
    <style type="text/css">
        #maindiv
        {
            height: 100%;
            background-image: url("Content/bg-theme.gif");
            background-repeat: repeat-y;
        }
        #pic
        {
            opacity: .3;
            filter: Alpha(opacity=50);
            -webkit-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            -moz-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
        }
        .lbs
        {
            font-family: 'Tangerine' , serif;
            font-size: 32px;
            text-shadow: 4px 4px 4px #CCCCCC;
            font-weight: bold;
        }
        .auto-style3 {
            width: 555px;
        }
        .auto-style7 {
            width: 155px;
        }
        .auto-style8 {
            width: 156px;
        }
        #terms {
            width: 957px;
            height: 293px;
        }
        </style>
    <%--    <script type="text/javascript">
        $(document).ready(function () {
            $("#ImageButton1").click(function () {
                if ($("#tbOrder").val() == '') {
                    alert("Please enter an Order Numer");
                    return false;
                } else {
                    $("#HiddenField1").val("1");
                }
            });

        });
    </script>--%>
    <script type="text/javascript">
       /* function submit()
        {
            $.ajax({
                headers: {  'Access-Control-Allow-Origin': '*' },
                type: "POST",
                url: "https://test.authorize.net/payment/payment",
                data: $("#token3").serialize(), // serializes the form's elements.
                success: function (data) {
                    alert(data); // show response from the php script.
                }
            });
        }
        function openpopup() {
            window.open("Terms.html ", '_blank')
        }
        function submit2() {
            $(SUBMIT).click();
            $(IFRAME_CONTAINER).show();

            /*$.ajax({
                type: "POST",
                url: "Step2.aspx/SubmitShippingInfo",
                //data: "{ example: '" + example + "'}",
               // contentType: "application/json; charset=utf-8",
                success: function (data) {
                    alert("Updated!");
                }
            });#1#

            PageMethods.SubmitShippingInfo()
        }*/

        $(document).ready(function () {
            $(":checkbox").prop('disabled', true);
            $(".AgreeToTerms").prop('disabled', true);
            
        });

        function enableNextButton() {
             $(".NextButton").prop('disabled', false);
        }
       
        $(document).ready(function () {

            setTimeout(srollIframe, 1000);

        });

        
        function srollIframe() {
            var iframe = $("#frame2").contents();

            $(iframe).scroll(function () {
                if ($(iframe).scrollTop() > $(iframe).height() - 1000) {
               //     alert("near bottom!");
                    $(":checkbox").prop('disabled', false);
                }
            });
        }
    </script>
    

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div style="margin-left: auto; margin-right: auto; width: 970px; background-color: #FFFFFF;" id="maindiv">
        <table style="width: 100%; font-family: Tahoma; height: 100%;">
            <tbody><tr>
                <td width="50px">
                </td>
                                <td width="50px">
                </td>
                <td colspan="3">
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td width="50px">
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="top">
                        <div id="imageWC" style="width: 130px; float: left;">
                            <img id="Image1" src="./Content/WCI_ColorLogo_clearbackground.jpg" style="height:100px;">
                        </div>
                        <div style="margin-left: 30px; float: left; top: 20px; font-family: &#39;Times New Roman&#39;;
                            height: 70px; margin-top: 15px; width: 500px;">
                            <b style="font-size: 28px">Wine Cellar Innovations</b>
                            
                            <br>
                            <b style="font-size: 22px">Order Processing Portal</b>
                        </div>
<%--                        <div style="margin-top: 70px; float: right; width: 30px; height: 30px;">
                            <input type="image" name="ImageButton2" id="ImageButton2" title="Close Web Page" src="./Content/exit.png" onclick="window.close();">
                        </div>--%>
                    </div>
                </td>
                <td width="50px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="auto-style7">
                    Main order #
                                  
                </td>
                <td class="auto-style8">
                    &nbsp;
                    <% =orderIdS %>

                </td>
                <td class="auto-style3">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Order reference
                </td>
                <td class="auto-style8">
                    &nbsp; 
                    <% =orderRef %>
                </td>
                <td class="auto-style3">
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                </td>
                <td class="auto-style8">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image2" src="./Content/number_2.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 5px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: -2px; top: 20px;">
                                                <span id="Label2" class="lbs" style="font-weight:normal;text-decoration:none;">Step 2:  Agree to Terms</span>
                                            </div>
                                            </td>
                <td>
                    &nbsp;
                </td>
            </tr>
           
            <tr>
                <td colspan="5" style="text-align: center">
                
                   <%-- <input type="submit" name="Button1" s value="View Terms " id="Button1" onclick="openpopup()" style="color:Maroon;font-size:20px;font-weight:bold; width: 152px; text-align: center"/></td>--%>
                
            </tr>
             </tbody></table>   
      <%--  <iframe class="cover" src="about:blank"></iframe>
        <div style="overflow-y: hidden; margin-right:auto; margin-left:auto;   height: 500px" >
           
            <iframe id="terms" src ="Terms.html?wmode=transparent"  />
        </div>
        --%>
        
        <div id="content" style="align-content: center; margin-left: 30px; overflow-x: hidden">
            <iframe src="Terms.html" name="frame2" id="frame2" frameborder="0"  marginwidth="0" marginheight="0" scrolling="auto"  width="900px" height="500px" onload="" allowtransparency="false"></iframe>
        </div>
        <div id="block"></div>
        <table style="width: 100%; font-family: Tahoma; height: 100%;">
            <tbody>
            <tr>
                <td colspan="5" style="text-align: center">
                    <asp:CheckBox runat="server" CssClass="AgreeToTerms" OnClick="enableNextButton();"   ID="AgreeToTerms" Width="200px" Text="Agree to terms"/>
                </td>
               
             
            </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                        <asp:Label runat="server" ID="ValidationMessage" ForeColor="Red" Width="400px"  Visible="False"  Text="Please agree to Terms to proceed" ></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>

                </tr>
                <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="Panel1" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ImageButton1&#39;)">
                        <div>
                            <div style="float: right; width: 128px; height: 128px;">
                          

                                <asp:ImageButton type="image" CssClass="NextButton"  name="NextButton"  ID="NextButton" src="./Content/ArrowRightn.png"  OnClick="NextButton_OnServerClick" runat="server" /> 
                            </div>
                            <div style="float: left; width: 128px; height: 128px;">
                                <asp:ImageButton type="image" name="PrewButton"  src="./Content/ArrowLeft.png" OnClick="PrevButton_OnServerClick"   runat="server" />
                            </div>
                           <%-- <div style="margin-top: 50px; margin-right: 15px;width: 125px; height: 30px; float: right;">
                                <asp:Button  style="height: 30px; width: 125px;" type="button" Text="Submit Information" 
                               
                                   OnClientClick="submit2();return false;"  runat="server"   /></div>--%>
                        </div>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </tbody></table>
        
       <%-- <asp:HtmlIframe runat="server" id="authorizeIframe" Visible="False">
            
        </asp:HtmlIframe>--%>
    </div>
   

    

 
</asp:Content>
    
<asp:Content runat="server" ContentPlaceHolderID="NoForm">

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Bottom">
    <div style="margin-left: auto; margin-right: auto; width: 920px;" id="maindiv2">
        <br>
        <div id="siteFooterLeft" style="border-color: #FFFFFF; font-size: small; border-top-style: solid;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      border-top-width: thin; border-bottom-style: solid; border-bottom-width: thin;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      background-color: #DBD5B9;" align="center">
            <br>
            JAMES L. DECKEBACH LTD. DBA Wine Cellar Innovations - <q>an Ohio corporation</q>
            <br>
            4575 Eastern Ave., Cincinnati, OH 45226 - 1-800-229-9813
            <br>
            <i>Copyright © 2014 Wine Cellar Innovations. All rights reserved.</i>
            <br>
            <br>
        </div>
    </div>

</asp:Content>








        
  
