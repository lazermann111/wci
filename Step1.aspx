﻿<%@ Page Title=""  EnableEventValidation="false"  Language="C#" MasterPageFile="~/StepsPages.master" AutoEventWireup="true" CodeBehind="Step1.aspx.cs" Inherits="WineCellarPayment.Step1" %>
<%@ Import Namespace="CoffeeShopWebApp" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">


    <title>
	WCI - Payment Portal
    </title>
       
    <style type="text/css">
        #maindiv
        {
            height: 100%;
            background-image: url("Content/bg-theme.gif");
            background-repeat: repeat-y;
        }
        #pic
        {
            opacity: .3;
            filter: Alpha(opacity=50);
            -webkit-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            -moz-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
        }
        .lbs
        {
            font-family: 'Tangerine' , serif;
            font-size: 32px;
            text-shadow: 4px 4px 4px #CCCCCC;
            font-weight: bold;
        }
        .auto-style6 {
            width: 300px;
           
        }
        .auto-style7 {
            width: 129px;
        }
        .auto-style8 {
            height: 24px;
        }
        .auto-style9 {
            width: 129px;
            height: 24px;
        }
    </style>
<%--    <script type="text/javascript">
        $(document).ready(function () {
            $("#ImageButton1").click(function () {
                if ($("#tbOrder").val() == '') {
                    alert("Please enter an Order Numer");
                    return false;
                } else {
                    $("#HiddenField1").val("1");
                }
            });

        });
    </script>--%>
</head>



<div class="aspNetHidden">
	</div>
    <div style="margin-left: auto; margin-right: auto; width: 970px; background-color: #FFFFFF;" id="maindiv">
        <table style="width: 100%; font-family: Tahoma; height: 100%;">
            <tbody><tr>
                <td width="50px">
                </td>
                                <td width="50px">
                </td>
                <td colspan="3">
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td width="50px">
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="top">
                        <div id="imageWC" style="width: 130px; float: left;">
                            <img id="Image1" src="./Content/WCI_ColorLogo_clearbackground.jpg" style="height:100px;">
                        </div>
                        <div style="margin-left: 30px; float: left; top: 20px; font-family: &#39;Times New Roman&#39;;
                            height: 70px; margin-top: 15px; width: 500px;">
                            <b style="font-size: 28px">Wine Cellar Innovations</b>
                            
                            <br>
                            <b style="font-size: 22px">Order Processing Portal</b>
                        </div>
<%--                        <div style="margin-top: 70px; float: right; width: 30px; height: 30px;">
                            <input type="image" name="ImageButton2" id="ImageButton2" title="Close Web Page" src="./Content/exit.png" onclick="window.close();">
                        </div>--%>
                    </div>
                </td>
                <td width="50px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="auto-style7">
                    Main order #
                                  
                </td>
                <td class="auto-style17">
                    &nbsp;
                    <% =orderIdS %>
</td>
                <td class="auto-style15">
                    &nbsp;
                </td>
                <td class="auto-style16">
                    &nbsp;
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td class="auto-style8">
                    &nbsp;
                </td>
                <td class="auto-style9">
                       </td>
                <td class="auto-style8">
                    <asp:Label runat="server" ID="ValidationMessage" ForeColor="Red"   Visible="False"  Text="Sorry, we cannot find such order. Please go back and enter valid order id " ></asp:Label>
            
                </td>
                <td class="auto-style8">
                    &nbsp;
                </td>
                <td class="auto-style8">
                </td>
                <td class="auto-style8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                </td>
                <td class="auto-style17">
                    &nbsp;
                </td>
                <td class="auto-style15">
                    &nbsp;
                </td>
                <td class="auto-style16">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image2" src="./Content/number_1.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 5px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: -11px; top: 18px;">
                                                <span id="Label2" class="lbs" style="font-weight:normal;text-decoration:none;">Step 1:  Verify your Sold To \ Bill To \ Ship To Information</span>
                                            </div>
                                            <div style="margin-top: 50px; margin-left: 100px; position: absolute; top: 12px; left: -7px;"auto-style3">
                                            Please review each field below to ensure the data is correct. Update if nesessary.
                                                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="auto-style18">
                </td>
                <td class="auto-style18">
                    &nbsp;
                </td>

                <td class="auto-style19">
                </td>
                <td class="auto-style20">
                    &nbsp; 
                </td>
                <td class="auto-style21">
                </td>
                <td class="auto-style18">
                    &nbsp;
                </td>
            </tr>
          <%--  <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    Balance Due:
                </td>
                <td class="auto-style17">
                <asp:TextBox id="Price_textBox"   class="auto-style6" type="text"   runat="server"/></td>
                <td class="auto-style15">
                    
                </td>
                <td class="auto-style16">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>--%>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                </td>
                <td class="auto-style4">
                    &nbsp; <u>Sold To Information</u>
                </td>
                <td class="auto-style15">
                    &nbsp; <u>Bill To Information</u>
                </td>
                <td>
                    &nbsp; <u>Ship To Information</u>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                </td>
                <td class="auto-style4">
                    <asp:DropDownList runat="server" ID="SoldToCopy" AutoPostBack="True"     OnSelectedIndexChanged="CopyOptionChanged">
                        <asp:ListItem Enabled="true" Text="Copy details.. " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Bill To Info" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Ship To Info" Value="2"></asp:ListItem>
                    </asp:DropDownList>  
                </td>
                <td class="auto-style15">
                    <asp:DropDownList runat="server" ID="BillToCopy"  AutoPostBack="True"  OnSelectedIndexChanged="CopyOptionChanged">
                        <asp:ListItem Enabled="true" Text="Copy details..  " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Ship To Info" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Sold To Info" Value="2"></asp:ListItem>
                    </asp:DropDownList>  
                    
                   <%-- <asp:Button Text="Go" OnClick="O" runat="server"/>--%>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ShipToCopy" AutoPostBack="True"    OnSelectedIndexChanged="CopyOptionChanged">
                        <asp:ListItem Enabled="true" Text="Copy details..  " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Bill To Info" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Copy from Sold To Info" Value="2"></asp:ListItem>
                    </asp:DropDownList>  
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Name:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Name"   class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Name"  class="auto-style6" type="text" runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Name"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Name:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="TextBox1"   class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="TextBox2"  class="auto-style6" type="text" runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="TextBox3"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Company:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Company"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Company"   class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Company"   class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Address 1:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_1"    class="auto-style6" type="text" runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_1"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_1"   class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Address 2:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_2"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_2"   class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_2"   class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Address 3:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_3"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_3"    class="auto-style6" type="text" runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_3"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    City:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_City"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_City"   class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_City"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    State:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_State"   class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_State"   class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_State"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Zip:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Zip"    class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Zip"   class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Zip"   class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    County:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_County"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15"> 
                    <asp:TextBox id="B_County"   class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_County"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Country:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Country"  class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Country"  class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Country"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Phone 1:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_1"  class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_1"  class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_1"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Phone 2:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_2"  class="auto-style6" type="text"  runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_2"  class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_2"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Fax:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Fax"  class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Fax"  class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Fax"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    Cell phone:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_cell"  class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_cell"  class="auto-style6" type="text" runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_cell"  class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style7">
                    E-mail:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_EMail"  class="auto-style6" type="text" runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Email"  class="auto-style6" type="text" runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Email"  style=" pointer-events: none;" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
        <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="Panel1" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ImageButton1&#39;)">
                        <div>
                           
                            <div style="float: left; width: 128px; height: 128px;">
                                <asp:ImageButton type="image" name="PrewButton" id="PrewButton2"  class="aspNetDisabled" src="./Content/ArrowLeft.png" OnClick="PrevButton_OnServerClick" runat="server"/>
                            </div>
                         <%--   <div style="margin-top: 50px; margin-right: 15px;width: 125px; height: 30px; float: right;">--%>
                                
                            <div style="float: right; width: 128px; height: 128px;">
                            <asp:ImageButton type="image" name="NextButton" ID="NextButton"  src="./Content/ArrowRightn.png" OnClick="SubmitButton_OnServerClick" runat="server" /> 
                                </div>
                                <%--<asp:Button   style="height: 30px; width: 135px;" type="button" Text="Proceed to payment" OnClick="SubmitButton_OnServerClick"  OnClientClick="submit2();return false;" runat="server" ID="Button1" /></div>--%>
                        </div>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            </tbody></table>
    </div>
    <div style="margin-left: auto; margin-right: auto; width: 920px;" id="maindiv2">
        <br>
        <div id="siteFooterLeft" style="border-color: #FFFFFF; font-size: small; border-top-style: solid;
            border-top-width: thin; border-bottom-style: solid; border-bottom-width: thin;
            background-color: #DBD5B9;" align="center">
            <br>
            JAMES L. DECKEBACH LTD. DBA Wine Cellar Innovations - <q>an Ohio corporation</q>
            <br>
            4575 Eastern Ave., Cincinnati, OH 45226 - 1-800-229-9813
            <br>
            <i>Copyright © 2014 Wine Cellar Innovations. All rights reserved.</i>
            <br>
            <br>
        </div>
    </div>
    
</form>



</asp:Content>