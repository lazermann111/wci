﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace WineCellarPayment.Helpers
{
    public class MailService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static bool SendMail(string from, string MailTo, string subject, string mailmessage)
        {
          
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            try
            {
                var mailLogin =  System.Configuration.ConfigurationManager.AppSettings["mail_login"];
                var mailName =  System.Configuration.ConfigurationManager.AppSettings["mail_name"];
                var mailPass =  System.Configuration.ConfigurationManager.AppSettings["mail_pass"];
                var mailhost =  System.Configuration.ConfigurationManager.AppSettings["mail_host"];
                var mailport = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["mail_port"]);
                var mail_enablessl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["mail_enablessl"])  ;
        
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(mailLogin, mailPass);
                smtpClient.Port = mailport;
                smtpClient.Host = mailhost;
                smtpClient.EnableSsl = mail_enablessl;

                var fromAddress = new MailAddress(from, mailName);
                message.IsBodyHtml = true;
                message.From = fromAddress;
                var toAddress = new MailAddress(MailTo);
                message.To.Add(toAddress);
                message.Subject = subject;
                message.Body = mailmessage;
                smtpClient.Send(message);
                return true;
            }
            catch (Exception e)
            {
                logger.Error(e.ToString);
                return false;
            }
            
        }


        public static String paymentReceivedMailTemplate(string customerName)
        {
            return "< p > Hi "+ customerName +",</ p >" +
   

               " < p > we received your payment.  </ p >"
              +
                     

               " < p > If you have any problems, please contact us </ p >"+
                     

               " < br />" +
                     

               " < p > This message is for informational purposes only.Please do not reply to this email.</ p >" +


                "< p > Thank You.</ p >";
        }

        public static string paymentReceivedMailTemplate2(string Username)
        {
            string EmailBody = string.Empty;
           // Logo = HttpContext.Current.Server.MapPath("App_Themes/images/logo.png");
            try
            {
                EmailBody = "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                            "<head>" +
                            "<meta http-equiv='Content-Type' content='text/html; charset='ISO-8859-1'>" +
                            "</head>" +
                            "<body>" +
                            "<table width=\"946\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                            "<tr>" +
                            "<td>" +
                            "<table width=\"946\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"inner_table\">" +
                            "<tr>" +
                            "<td>" +
                            "<table width=\"867\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">" +
                            "<tr>" +
                            "<td><table width=\"100%\" style=\"background:url('')no-repeat scroll left top #fff; padding:7px;\"><tr><td>" +
                            "<a style='float:left; color:blue' ><img src='' border=\'0\' /></a><br/>" +
                            "</td></tr></tbody></table></td>" +
                            "</tr><tr><td><br/><br/></td></tr>" +
                            "<tr>" +
                            "<td valign=\"top\">" +
                            "<table style=\"background-color:white;color:black;padding:10px; font-size:20px;\" width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">" +
                            "<tr>" +
                            "<td>" +
                            "<p style=\"font-family:Times New Roman;\">" +
                            "</p>" +
                            "<p style=\"font-family:Times New Roman\">" +

                            "Dear " + Username + ",</p>" +
                            "<p style=\"font-family:Times New Roman\">" +
                            " We received your payment.</p>" +

                            "<p style=\"font-family:Times New Roman\"></p>" +
                            "<p  style=\"font-family:Times New Roman;\">Sincerely, <br/>" +
                            " WCI team </p>" +
                            "</td>" +
                            "</tr>" +
                            "</table>" +
                            "</td>" +
                            "</tr>" +

                            "</table>" +
                            "</td>" +
                            "</tr>" +
                            "</table>" +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "</tr>" +
                            "</table>" +
                            "</body>" +
                            "</html>";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EmailBody;
        }

    }



}