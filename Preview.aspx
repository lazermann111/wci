﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/StepsPages.master" AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="WineCellarPayment.Preview" %>


<asp:Content ContentPlaceHolderID="TitlePlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Tangerine" />
    <style type="text/css">
        #maindiv
        {
            height: 100%;
            background-image: url("./Content/bg-theme.gif");
            background-repeat: repeat-y;
        }
        .pic
        {
            opacity: .5;
            filter: Alpha(opacity=50);
            -webkit-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            -moz-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
        }
        .lbs
        {
            font-family: 'Tangerine' , serif;
            font-size: 48px;
            text-shadow: 4px 4px 4px #CCCCCC;
            font-weight: bold;
            color:black
        }
        .auto-style1 {
            
        }
    </style>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">





    <div style="margin-left: auto; margin-right: auto; width: 970px; background-color: #FFFFFF;" id="maindiv">
        <table style="width: 100%; font-family: Tahoma; height: 100%;">
            <tbody><tr>
                <td width="50px">
                </td>
                <td colspan="3">
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td width="50px">
                    &nbsp;
                </td>
                <td colspan="3">
                    <div id="top">
                        <div id="imageWC" style="width: 130px; float: left;">
                            <img id="Image1" src="./Content/WCI_ColorLogo_clearbackground.jpg" style="height:100px;">
                        </div>
                        <div style="margin-left: 30px; float: left; top: 20px; font-family: &#39;Times New Roman&#39;;
                            height: 70px; margin-top: 15px; width: 500px;">
                            <b style="font-size: 28px">Wine Cellar Innovations</b>
                            
                            <br>
                            <b style="font-size: 22px">Order Processing Portal</b>
                        </div>
                        <div style="margin-top: 70px; float: right; width: 30px; height: 30px;">
                            <input type="image" name="ImageButton2" id="ImageButton2" title="Close Web Page" src="./Content/exit.png" onclick="window.close();">
                        </div>
                    </div>
                </td>
                <td width="50px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="3" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                </td>
                <td align="center" colspan="3" class="auto-style1">
                    <div style="position: relative; width: 880px; height: 600px; top: -4px; left: 0px;">
                        <div id="pic" style="position: absolute; top: 0px; left: 0px; z-index: 1;">
                            <img id="Image5" src="./Content/wineroom.jpg" style="width:880px; opacity:0.4; height: 610px;"/>
                        </div>
                        <div style="position: absolute; top: 100px; left: 10px">
                            <table>
                                <tbody><tr>
                                    <td align="center">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="margin-top: 25px; margin-left: 110px; position: absolute; z-index: 3;
                                                left: -96px; top: 0px;">
                                                <span id="Label1" class="lbs" style="color:black;font-size:58px;font-weight:normal;">Process your Order in 3 Easy Steps...</span>
                                            </div>          
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image2" src="./Content/number_1.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 25px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: 0px; top: 0px;">
                                                <span id="Label2" class="lbs" style="font-weight:normal;text-decoration:none;">Verify your Sold To \ Bill To\ Ship To Information</span>
                                            </div>
                                           
                                        </div>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image6" src="./Content/number_2.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 25px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: 0px; top: 0px;">
                                                <span id="Label6" class="lbs" style="font-weight:normal;text-decoration:none;">Agree to Terms</span>
                                            </div>
                                            
                                        </div>
                                    </td>
                                </tr>
                               
                              <%--  <tr>
                                    <td>
                                        <div style="position: relative; top: 4px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image7" src="./Content/number_3.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 25px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: 0px; top: 0px;">
                                                <span id="Label8" class="lbs" style="font-weight:normal;text-decoration:none;">Review you order details</span>
                                            </div>
                                           
                                        </div>
                                    </td>
                                </tr>
                                    --%>
                                <tr>
                                    <td height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image6" src="./Content/number_3.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 25px; margin-left: 100px; position: absolute; z-index: 3;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          left: 0px; top: 0px;">
                                                <span id="Label6" class="lbs" style="font-weight:normal;text-decoration:none;">Submit your payment</span>
                                            </div>
                                            
                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                    </div>
                </td>
                <td class="auto-style1">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="3">
                    <div id="Panel1" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ImageButton1&#39;)">
	
                        <div>
                            <div style="float: right; width: 128px; height: 128px;">
                                <input type="image" name="ImageButton1"  id="ImageButton1"  src="./Content/ArrowRightn.png" OnServerClick="ImageButton1_OnServerClick" runat="server"></div>
                            <div style="margin-top: 50px; width: 125px; height: 50px; float: right; margin-right: 15px;">
                                <asp:TextBox runat="server" name="tbOrder"   ID="tbOrder" title="Numeric Only" CssClass="" style="background-color:#FFEBAE;width:120px;text-align:center; height: 22px;" OnTextChanged="tbOrder_TextChanged"/></div>
                            <div style="margin-top: 50px; width: 290px; height: 50px; float: right; margin-right: 15px;" align="right">
                                <span id="Label36" style="display:inline-block;color:#7E0000;font-size:14pt;font-weight:normal;width:280px;">Enter Your Main Order #:</span>
                            </div>
                        </div>
                    
</div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <input type="hidden" name="HiddenField1" id="HiddenField1" value="0">
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </tbody></table>
    </div>
    <div style="margin-left: auto; margin-right: auto; width: 920px;" id="maindiv2">
        <br>
        <div id="siteFooterLeft" style="border-color: #FFFFFF; font-size: small; border-top-style: solid;
            border-top-width: thin; border-bottom-style: solid; border-bottom-width: thin;
            background-color: #DBD5B9;" align="center">
            <br>
            JAMES L. DECKEBACH LTD. DBA Wine Cellar Innovations - <q>an Ohio corporation</q>
            <br>
            4575 Eastern Ave., Cincinnati, OH 45226 - 1-800-229-9813
            <br>
            <i>Copyright © 2014 Wine Cellar Innovations. All rights reserved.</i>
            <br>
            <br>
        </div>
    </div>
    

</asp:Content>



