﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WineCellarPayment
{
    public partial class Preview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string OrderID = "";

        protected void ImageButton1_OnServerClick(object sender, ImageClickEventArgs e)
        {
            OrderID = tbOrder.Text;
            int orderId;
            if (OrderID.Equals(String.Empty) || !Int32.TryParse(OrderID, out orderId)) return;


            Session["order_id"] = orderId;
            Response.Redirect("Step1.aspx");
        }

        protected void tbOrder_TextChanged(object sender, EventArgs e)
        {

        }
    }
}