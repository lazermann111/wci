﻿<%@ Page Language="C#" MasterPageFile="~/StepsPages.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Step3.aspx.cs" Inherits="WineCellarPayment.Step3" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.HtmlControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Import Namespace="CoffeeShopWebApp" %>

<asp:Content ContentPlaceHolderID="TitlePlaceHolder" runat="server">
    <title>
        WCI - Payment Portal
    </title>
    <link rel="stylesheet" type="text/css" href="./Content/css"/>


    
    <style type="text/css">
        #maindiv
        {
            height: 100%;
            background-image: url("Content/bg-theme.gif");
            background-repeat: repeat-y;
        }
        #pic
        {
            opacity: .3;
            filter: Alpha(opacity=50);
            -webkit-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            -moz-box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
            box-shadow: 0px 10px 7px rgba(0,0,0,0.4), 0px 20px 10px rgba(0,0,0,0.2);
        }
        .lbs
        {
            font-family: 'Tangerine' , serif;
            font-size: 32px;
            text-shadow: 4px 4px 4px #CCCCCC;
            font-weight: bold;
        }
        .auto-style6 {
            width: 300px;
        }
        .auto-style15 {
            width: 307px;
        }
        .auto-style16 {
            width: 255px;
            font-size: 12px;
            font-style:italic;
            color:red;
        }
        .auto-style17 {
            width: 315px;
        }
        .auto-style18 {
            height: 24px;
        }
        .auto-style19 {
            width: 315px;
            height: 24px;
        }
        .auto-style20 {
            width: 307px;
            height: 24px;
        }
        .auto-style21 {
            width: 255px;
            font-size: 12px;
            font-style: italic;
            color: red;
            height: 24px;
        }
        .auto-style26 {
            width: 202px;
        }
        .auto-style27 {
            height: 24px;
            width: 202px;
        }
        </style>
    <script type="text/javascript">
        function submit2() {
            $(SUBMIT).click();
            $(IFRAME_CONTAINER).show();
        }
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   



    <div style="margin-left: auto; margin-right: auto; width: 970px; background-color: #FFFFFF;" id="maindiv">
        <table style="width: 100%; font-family: Tahoma; height: 100%;">
            <tbody><tr>
                <td width="50px">
                </td>
                                <td class="auto-style26">
                </td>
                <td colspan="3">
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td width="50px">
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="top">
                        <div id="imageWC" style="width: 130px; float: left;">
                            <img id="Image1" src="./Content/WCI_ColorLogo_clearbackground.jpg" style="height:100px;">
                        </div>
                        <div style="margin-left: 30px; float: left; top: 20px; font-family: &#39;Times New Roman&#39;;
                            height: 70px; margin-top: 15px; width: 500px;">
                            <b style="font-size: 28px">Wine Cellar Innovations</b>
                            
                            <br>
                            <b style="font-size: 22px">Order Processing Portal</b>
                        </div>
<%--                        <div style="margin-top: 70px; float: right; width: 30px; height: 30px;">
                            <input type="image" name="ImageButton2" id="ImageButton2" title="Close Web Page" src="./Content/exit.png" onclick="window.close();">
                        </div>--%>
                    </div>
                </td>
                <td width="50px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="auto-style26">
                    Main order #
                                  
                </td>
                <td class="auto-style17">
                    &nbsp;
                    <% =orderIdS %>
</td>
                <td class="auto-style15">
                    &nbsp;
                </td>
                <td class="auto-style16">
                    &nbsp;
                </td>
                <td width="50px">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Order referece
                </td>
                <td class="auto-style17">
                    &nbsp; 
                    <% =orderRef %>
                </td>
                <td class="auto-style15">
                    &nbsp;
                </td>
                <td class="auto-style16">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                </td>
                <td class="auto-style17">
                    &nbsp;
                </td>
                <td class="auto-style15">
                    &nbsp;
                </td>
                <td class="auto-style16">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="4" height="80px">
                                        <div style="position: relative; top: 0px; left: 0px; width: 900px; height: 80px;">
                                            <div style="position: absolute; top: 0px; left: 0px; z-index: 2;">
                                                <img id="Image2" src="./Content/number_3.png" style="height:80px;width:80px;">
                                            </div>
                                            <div style="margin-top: 5px; margin-left: 100px; position: absolute; z-index: 3;
                                                left: 0px; top: 20px;">
                                                <span id="Label2" class="lbs" style="font-weight:normal;text-decoration:none;">Step 3:&nbsp; Review your order details and submit payment</span>
                                            </div>
                                                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="auto-style18">
                </td>
                <td class="auto-style27">
                    &nbsp;
                </td>

                <td class="auto-style19">
                </td>
                <td class="auto-style20">
                    &nbsp; 
                </td>
                <td class="auto-style21">
                </td>
                <td class="auto-style18">
                    &nbsp;
                </td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                </td>
                <td class="auto-style4">
                    &nbsp; <u>Sold To Information</u>
                </td>
                <td class="auto-style15">
                    &nbsp; <u>Bill To Information</u>
                </td>
                <td>
                    &nbsp; <u>Ship To Information</u>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Name:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Name"  Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Name" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Name"  Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Company:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Company"  Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Company"  Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Company"  Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Address 1:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_1"  Enabled="False"  class="auto-style6" type="text" runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_1" Enabled="False"  class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_1" Enabled="False"  class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Address 2:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_2"  Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_2"  Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_2"  Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Address 3:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Address_3"  Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Address_3"   Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Address_3"  Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    City:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_City"  Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_City"  Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_City"  Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    State:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_State"  Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_State"  Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_State"  Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Zip:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Zip"   Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Zip"  Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Zip"  Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    County:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_County"  Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15"> 
                    <asp:TextBox id="B_County" Enabled="False"  class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_County" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Country:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Country" Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Country" Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Country" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Phone 1:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_1" Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_1" Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_1" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Phone 2:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_2" Enabled="False" class="auto-style6" type="text"  runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_2" Enabled="False" class="auto-style6" type="text"   runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_2" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Fax:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Fax" Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Fax" Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td>
                    <asp:TextBox id="Sp_Fax" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    Cell phone:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_Phone_cell" Enabled="False" class="auto-style6" type="text"  runat="server"/>
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Phone_cell" Enabled="False" class="auto-style6" type="text" runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Phone_cell" Enabled="False" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    E-mail:
                </td>
                <td class="auto-style4">
                    <asp:TextBox id="S_EMail" Enabled="False" class="auto-style6" type="text" runat="server" />
                </td>
                <td class="auto-style15">
                    <asp:TextBox id="B_Email" Enabled="False" class="auto-style6" type="text" runat="server" />
                </td>
                <td>
                    <asp:TextBox id="Sp_Email" Enabled="False" style=" pointer-events: none;" class="auto-style6" type="text" runat="server"/>
                </td>
            </tr>
                
                
            <tr>
                <td class="auto-style22">
                    &nbsp;
                </td>
                <td class="auto-style28">
                    Balance Due:
                </td>
                <td class="auto-style23">
                    <asp:TextBox id="Price_textBox"  Enabled="False" class="auto-style6" type="text"   runat="server"/></td>
                <td class="auto-style24">
                    
                </td>
                <td class="auto-style25">
                </td>
                <td class="auto-style22">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    <asp:Label runat="server" ID="Label1" Text="Payment Options:"> </asp:Label>
                </td>
                <td class="auto-style17">
                    <asp:DropDownList runat="server" ID="PaymentOptionsDropdown" AutoPostBack="True"  OnSelectedIndexChanged="PaymentOptionChanged">
                        <asp:ListItem Enabled="true" Text="Select " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Credit card/Wire transfer" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Paypal" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Check/Cash" Value="3"></asp:ListItem>
                    </asp:DropDownList>   
                </td>
                <td class="auto-style15">
                    
                </td>
                <td class="auto-style16">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    <asp:Label runat="server" ID="PaymentAmount" Text="Amount:"> </asp:Label>
                </td>
                <td class="auto-style17">
                    <asp:DropDownList runat="server" ID="PaymentAmountDropdown" AutoPostBack="True"  OnSelectedIndexChanged="PaymentAmountChanged">
                        <%-- <asp:ListItem Enabled="true" Text="Select " Value="-1"></asp:ListItem>--%>
                        <asp:ListItem Text="Make full payment" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Make partial payment" Value="2"></asp:ListItem>
                    </asp:DropDownList>   
                </td>
                <td class="auto-style15">
                    
                </td>
                <td class="auto-style16">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="auto-style26">
                    <asp:Label runat="server" ID="customAmountLabel" Text="Amount to pay:"   Visible="False"></asp:Label>
                </td>
                <td class="auto-style17">
                    <asp:TextBox   ID="customAmountInputBox" AutoPostBack="True"  OnTextChanged="customAmountInputBox_OnTextChanged"   Enabled="True" Visible="False" class="auto-style6" type="text"   runat="server"/></td>
                <%--  <asp:CustomValidator runat="server" ID="rngPrice" ControlToValidate="customAmountInputBox"
                    OnServerValidate="rngPrice_OnServerValidate"
                    ErrorMessage="Please enter valid amount to pay" />--%>
                
                     
                <td class="auto-style15">
                    <asp:Label runat="server" ID="ValidationMessage" ForeColor="Red"   Visible="False"  Text="Please enter valid amount to pay" ></asp:Label>
                </td>
                <td class="auto-style16">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td colspan="3">
                    <asp:Label ID="PaymentInstrucions" Visible="False" runat="server"></asp:Label>
                </td>
              
            </tr>
                
                 <tr runat="server" id="cardDetailsPanel" Visible="False">
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td colspan="2">
                    
                    
                        <div style = 'float:left;width:250px;'>
                            <label>Credit Card Number</label>
                            <div id = 'CreditCardNumber'>
                                <asp:TextBox runat="server"  type = 'text' size = '28' name = 'x_card_num' value = '4111111111111111' id = 'x_card_num'/>
                            </div>
                        </div>	
                        <div style = 'float:left;width:70px;'>
                            <label>Exp.</label>
                            <div id = 'CreditCardExpiration'>
                                <asp:TextBox runat="server" type = 'text' size = '5' maxlength = '5' name = 'x_exp_date' value = '0116' id = 'x_exp_date'/>
                            </div>
                        </div>
                        <div style = 'float:left;width:70px;'>
                            <label>CCV</label>
                            <div id = 'CCV'>
                                <asp:TextBox runat="server" type = 'password' size = '5' maxlength = '5' name = 'x_card_code' id = 'x_card_code' value = '123' />
                            </div>
                        </div>
                    </p>
                </td>
                     <td>
                         <asp:Label runat="server" ID="AuthorizeResponse" ForeColor="Red"   Visible="False"  Text="" ></asp:Label>
                     </td>  
                    
              
            </tr>
          
            <tr>
                <td>
                </td>
                <td colspan="4" style="border-top-style: double; border-width: thick; border-color: #941F30" valign="bottom" align="center">
                </td>
                <td>
                </td>
            </tr>
        <tr>
            

                <td>
                    &nbsp;
                </td>
                <td colspan="4">
                    <div id="Panel1" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ImageButton1&#39;)">
                        <div>
                           <%--<div style="float: right; width: 128px; height: 128px;">
                                <asp:ImageButton type="image" name="NextButton" id="NextButton1" class="aspNetDisabled" src="./Content/ArrowRightn.png" OnClick="NextButton_OnServerClick" runat="server"/>
                            </div>#1#--%>
                            <div style="float: left; width: 128px; height: 128px;">
                                <asp:ImageButton type="image" name="PrewButton" id="PrewButton2"  class="aspNetDisabled" src="./Content/ArrowLeft.png" OnClick="PrevButton_OnServerClick" runat="server"/>
                            </div>
                          
                            <div style="float: right; width: 158px; height: 128px; align-items: center">
                                <asp:Button ID="SubmitPaymentButton" runat="server" OnClick="SubmitButton_OnServerClick" 
                                    style="margin-top: 50px; height: 30px; width: 135px; margin-right: 20px" Text="Submit" type="button" Visible="False" />
                                <asp:ImageButton ID="AuthorizePaymentButton" runat="server"  src="Content/purchase.png" OnClick="AuthorizePaymentButton_OnClick" style="margin-top: 50px;height: 30px; width: 135px;" type="image" Visible="False" />
                            </div>
                               
                        </div>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            </tbody></table>
    </div>
  


</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="NoForm">
   
    <%=this.HostedFormStart() %>
   
    <%=this.HostedFormEnd() %>  
    

   
    <div id="IFRAME_CONTAINER" style="display: none; margin-left: auto; margin-right: auto; width: 940px; background-color: rgb(211, 201, 166);">
        <iframe id="payframe" name="payframe"  style="  height: 800px; width: 100%; border-style: none;">
       
        </iframe>
    </div>
 
    <div style="margin-left: auto; margin-right: auto; width: 920px;" id="maindiv2">
        <br>
        <div id="siteFooterLeft" style="border-color: #FFFFFF; font-size: small; border-top-style: solid;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          border-top-width: thin; border-bottom-style: solid; border-bottom-width: thin;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          background-color: #DBD5B9;" align="center">
            <br>
            JAMES L. DECKEBACH LTD. DBA Wine Cellar Innovations - <q>an Ohio corporation</q>
            <br>
            4575 Eastern Ave., Cincinnati, OH 45226 - 1-800-229-9813
            <br>
            <i>Copyright © 2014 Wine Cellar Innovations. All rights reserved.</i>
            <br>
            <br>
        </div>
    </div>

</asp:Content>
