﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;

using WineCellarPayment.Model;
using WineCellarPayment.Helpers;


namespace WineCellarPayment
{
    public partial class Step4 : System.Web.UI.Page
    {
        public string orderIdS;
        public string orderRef;

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        String token = String.Empty;
        public decimal Price = 0;
        public decimal PartialPrice = 0;
        public OrderInfo BillToInfo = new OrderInfo();
        public OrderInfo SoldToInfo = new OrderInfo();
        public OrderInfo ShipToInfo = new OrderInfo();

        protected void Page_Load(object sender, EventArgs e)
        {

            // if (Page.IsPostBack) return;


            ShipToInfo = Session["ShipToInfo"] as OrderInfo;
            SoldToInfo = Session["SoldToInfo"] as OrderInfo;
            BillToInfo = Session["BillToInfo"] as OrderInfo;
            var p = Session["Price"];
            if (p != null) Price = Convert.ToDecimal(p);
            var orderId = (int) Session["order_id"];

           

            var partialPrice = Session["PartialPrice"];
            if (partialPrice != null)
            {
                PartialPrice = Convert.ToDecimal(partialPrice);
                customAmountInputBox.Text = PartialPrice.ToString();
                customAmountInputBox.Visible = true;
                PaymentOptionsDropdown.SelectedIndex = 1;


            }
            Session["PartialPrice"] = null;

             PopulateTextBoxes();
             loadIframe(partialPrice != null ? PartialPrice : Price, orderId.ToString());


        }
        
        protected void PrevButton_OnServerClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Step2.aspx");
        }

        private void SubmitInfoToDb()
        {
            String SQL =
                "INSERT INTO ord_cc_portal_soldto (cc_ord_serial, cc_bill_name,  cc_bill_addr1, " +
                "cc_bill_addr2, cc_bill_addr3, cc_bill_city,cc_bill_state, cc_bill_zip,  cc_bill_company, cc_bill_county, cc_bill_country, cc_bill_ph1, " +
                "cc_bill_ph2, cc_bill_fax, " +

                "   cc_bill_cell, cc_bill_email, cc_bill_contact_serial, cc_bill_contact_from,  cc_sold_name, cc_sold_company, " +
                "cc_sold_addr1, cc_sold_addr2, cc_sold_addr3, cc_sold_city, cc_sold_state, cc_sold_zip, cc_sold_county, cc_sold_country, cc_sold_ph1, cc_sold_ph2, " +
                "cc_sold_fax, cc_sold_cell, cc_sold_email, cc_sold_contact_serial, cc_modified, cc_modified_billto, cc_submit_reason, cc_processed, cc_approved  ) " +
                "VALUES (";

            SQL += "'" + Session["order_id"] + "',";
            SQL += "'" + BillToInfo.Name + "',";
            SQL += "'" + BillToInfo.Address1 + "',";
            SQL += "'" + BillToInfo.Address2 + "',";
            SQL += "'" + BillToInfo.Address3 + "',";
            SQL += "'" + BillToInfo.City + "',";
            SQL += "'" + BillToInfo.State + "',";
            SQL += "'" + BillToInfo.Zip + "',";
            SQL += "'" + BillToInfo.Company + "',";
            SQL += "'" + BillToInfo.County + "',";
            SQL += "'" + BillToInfo.Country + "',";

            SQL += "'" + BillToInfo.Phone1 + "',";
            SQL += "'" + BillToInfo.Phone2 + "',";
            SQL += "'" + BillToInfo.Fax + "',";
            SQL += "'" + BillToInfo.Cell_phone + "',";
            SQL += "'" + BillToInfo.email + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + "soldid" + "',";

            SQL += "'" + SoldToInfo.Name + "',";
            SQL += "'" + SoldToInfo.Company + "',";

            SQL += "'" + SoldToInfo.Address1 + "',";
            SQL += "'" + SoldToInfo.Address2 + "',";
            SQL += "'" + SoldToInfo.Address3 + "',";
            SQL += "'" + SoldToInfo.City + "',";
            SQL += "'" + SoldToInfo.State + "',";
            SQL += "'" + SoldToInfo.Zip + "',";
            SQL += "'" + SoldToInfo.County + "',";
            SQL += "'" + SoldToInfo.Country + "',";

            SQL += "'" + SoldToInfo.Phone1 + "',";
            SQL += "'" + SoldToInfo.Phone2 + "',";
            SQL += "'" + SoldToInfo.Fax + "',";
            SQL += "'" + SoldToInfo.Cell_phone + "',";

            SQL += "'" + SoldToInfo.email + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 1 + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "')";





            SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, SQL);


            SQL =
                " INSERT INTO ord_cc_portal_shipto(cc_ord_serial, cc_ord_rev, cc_ord_from_rev, cc_ship_name, cc_ship_company, cc_ship_addr1," +
                " cc_ship_addr2, cc_ship_addr3, cc_ship_city, cc_ship_state, cc_ship_zip, cc_ship_county, cc_ship_country, cc_ship_ph1, cc_ship_ph2," +
                " cc_ship_fax, cc_ship_cell, cc_ship_email, cc_ship_contact_serial, cc_ship_modified, cc_ship_processed, cc_approved  ) VALUES(";


            SQL += "'" + Session["order_id"] + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + ShipToInfo.Name + "',"; //cc_ship_name
            SQL += "'" + ShipToInfo.Company + "',";

            SQL += "'" + ShipToInfo.Address1 + "',";
            SQL += "'" + ShipToInfo.Address2 + "',";
            SQL += "'" + ShipToInfo.Address3 + "',";

            SQL += "'" + ShipToInfo.City + "',";
            SQL += "'" + ShipToInfo.State + "',";
            SQL += "'" + ShipToInfo.Zip + "',";
            SQL += "'" + ShipToInfo.County + "',";
            SQL += "'" + ShipToInfo.Country + "',";

            SQL += "'" + ShipToInfo.Phone1 + "',";
            SQL += "'" + ShipToInfo.Phone2 + "',";
            SQL += "'" + ShipToInfo.Fax + "',";
            SQL += "'" + ShipToInfo.Cell_phone + "',";
            SQL += "'" + ShipToInfo.email + "',";




            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "',";
            SQL += "'" + 0 + "',";

            SQL += "'" + 0 + "')";


            SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, SQL);
        }

        private void PopulateTextBoxes()
        {
            Price_textBox.Text = Price.ToString("#.##");
        }



        protected void SubmitButton_OnServerClick(object sender, EventArgs e)
        {
            try
            {
                SubmitInfoToDb();
                
            }
            catch (Exception e2)
            {
                logger.Error(e.ToString);
            }
        }

        private void loadIframe(decimal pr, string order_id)
        {
           /* var login = ConfigurationManager.AppSettings["ApiLogin"];
            var transactionKey = ConfigurationManager.AppSettings["TransactionKey"];
            var response = AuthorizeNetHelpers. getHostedPaymentPageResponse(login, transactionKey, pr, order_id);
            token = response.token;*/
        }



      
        protected string HostedFormStart()
        {
            if (token == null|| token.Equals(String.Empty)) return "";

            var sbForm = new StringBuilder("");

            sbForm.Append("<form action = '" + "https://test.authorize.net/payment/payment" + "' method = 'post'  target ='payframe'>\n");
            sbForm.Append("\t\t<input type = 'submit' id='SUBMIT' style='display:none'  value = 'Submit' />\n");

            sbForm.Append("\t\t<input type = 'hidden' style='display:none' name = 'token' value = '" + token + "' />\n");
            return sbForm.ToString();
        }

        protected string HostedFormEnd()
        {
            if (token == null || token.Equals(String.Empty)) return "";


            return "</form>";

        }


     

        protected void PaymentAmountChanged(object sender, EventArgs e)
        {
            switch (PaymentAmountDropdown.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    customAmountInputBox.Visible = false;
                    customAmountLabel.Visible = false;
                    Session["PartialPrice"] = null;
                    break;
                case 2:
                    customAmountInputBox.Visible = true;
                    customAmountLabel.Visible = true;
                    break;
            }
           
        }

        protected void customAmountInputBox_OnTextChanged(object sender, EventArgs e)
        {


         //   Price = Convert.ToDecimal(customAmountInputBox.Text);

            try
            {
                var p = Convert.ToDecimal(customAmountInputBox.Text);
                if (p > 0 && p <= Price)
                {
                    //Price = p;
                    ValidationMessage.Visible = false;
                    Session["PartialPrice"] = p;

                    Response.Redirect("Step4.aspx");
                }
                else
                {
                    ValidationMessage.Visible = true;
                }
            }
            catch
            {
                ValidationMessage.Visible = true;
            }
        }

        protected void SubmitButton_OnServerClick(object sender, ImageClickEventArgs e)
        {

        }

        protected void PaymentOptionChanged(object sender, EventArgs e)
        {
            switch (PaymentOptionsDropdown.SelectedIndex)
            {
                case 0:
                    break;
                case 1: //cc/wire
                    AuthorizePaymentButton.Visible = true;
                    SubmitPaymentButton.Visible = false;

                    PaymentInstrucions.Visible = false;
                    break;
                case 2: //paypal
                    AuthorizePaymentButton.Visible = false;
                    SubmitPaymentButton.Visible = true;

                    PaymentInstrucions.Visible = true;
                    PaymentInstrucions.Text = "Our Paypal is payments@wci.com";

                    break;
                case 3: //check
                    AuthorizePaymentButton.Visible = false;
                    SubmitPaymentButton.Visible = true;

                    PaymentInstrucions.Visible = true;
                    PaymentInstrucions.Text = "Our address is";

                    break;
            }
        }
    }
}
