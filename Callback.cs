


using System;
using System.Data;
using System.Data.SqlClient;
using WineCellarPayment.Helpers;

public class CallBack
{
    private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

    public static void PaymentCallBack(decimal amount, int order_id, string payment_method, string card_number)
    {
        try
        {

            string customer_details = "select * from ord_file where ord_serial=" + order_id;
            int cust_id = 0;


            string customer_name=null;
            string customer_email=null;
            DataSet obj = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, customer_details);
            //DataSet obj2 = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, customer_id);
            if (obj.Tables.Count > 0)
            {
                var dt = obj.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    var row = dt.Rows[0];
                    cust_id =  !Equals(row["ord_sold_cust_id"], string.Empty) ? Convert.ToInt32(row["ord_sold_cust_id"]) : 0;
                    customer_name = row["ord_sold_name"] as string;
                    customer_email = row["ord_sold_email"] as string;

                }

                //else return;
            }

            // registering new payment
            string cmd = "insert into pay_file (pay_cust_id,pay_date,pay_amt,pay_num,pay_method) values(";
            cmd += "'" + cust_id + "',";
            cmd += "'" + DateTime.Now + "',";
            cmd += "'" + amount + "',";
            cmd += "'" + card_number + "',";
            cmd += "'" + payment_method + "')";

            SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, cmd);




            cmd = "insert into pay_itm (pay_itm_ord_gen_serial,pay_itm_amt) values (";
            cmd += "'" + order_id + "',";    
            cmd += "'" + amount + "')";

            SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, cmd);

            // retrieving payed sum
            cmd = "select sum(pay_itm_amt) as pay from pay_itm where pay_itm_ord_gen_serial=" + order_id;
            var summ = SqlHelper.ExecuteScalar(SqlHelper.ConnectionString, CommandType.Text, cmd);


            // updating actual balance 
            cmd = "update ord_gen set ord_gen_balance=ord_gen_total - (" + summ + ") where ord_gen_serial=" + order_id;
            SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, cmd);



           

            var mailBody = MailService.paymentReceivedMailTemplate2(customer_name);
            var mailFrom = System.Configuration.ConfigurationManager.AppSettings["mail_from"];

            customer_email = "igor.lazarev@myprogrammer.com"; //todo remove
            MailService.SendMail(mailFrom, customer_email, "Your order on WCI", mailBody);
        }
        catch (Exception e)
        {
            logger.Error(e.ToString);
        }


    }


}

