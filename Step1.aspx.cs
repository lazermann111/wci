﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WineCellarPayment.Model;

namespace WineCellarPayment
{
    public partial class Step1 : System.Web.UI.Page
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public string orderIdS;
        //ValidationMessagepublic string orderRef;

        public OrderInfo ShipToInfo;
        public OrderInfo BillToInfo;
        public OrderInfo SoldToInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["copy_redirect"] != null && Session["copy_redirect"].Equals(Boolean.TrueString))
            {
                if (Session["ShipToInfo"] != null)
                {
                    ShipToInfo = (OrderInfo) Session["ShipToInfo"];
                }
                else
                {
                    ShipToInfo = new OrderInfo();
                }
                if (Session["BillToInfo"] != null)
                {
                    BillToInfo = (OrderInfo) Session["BillToInfo"];
                }
                else
                {
                    BillToInfo = new OrderInfo();
                }
                if (Session["SoldToInfo"] != null)
                {
                    SoldToInfo = (OrderInfo) Session["SoldToInfo"];
                }
                else
                {
                    SoldToInfo = new OrderInfo();
                }
                PopulateTextBoxes();
              //  Session["copy_redirect"] = Boolean.FalseString;
            }
             if (Page.IsPostBack) return;
            if(Session["BillToInfo"] != null || Session["ShipToInfo"] != null || Session["SoldToInfo"] != null) return;

            var orderId =(int) Session["order_id"];   
           

            try
            {
                  GetDbInfo(orderId);
                  PopulateTextBoxes();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString);
            }



        }

        private void GetDbInfo(object orderId)
        {
            orderIdS = orderId.ToString();

         
            // Initially Billto same as Sold To

           /* string bill_to =
                "SELECT  dbo.cont_file.cont_name, dbo.cont_file.cont_company, dbo.cont_file.cont_addr1, dbo.cont_file.cont_addr2, dbo.cont_file.cont_addr3, dbo.cont_file.cont_city, " +
                "dbo.cont_file.cont_state, dbo.cont_file.cont_zip, dbo.cont_file.cont_county, dbo.cont_file.cont_country, dbo.cont_file.cont_phone1, dbo.cont_file.cont_phone2, " +
                "dbo.cont_file.cont_fax, dbo.cont_file.cont_cell, dbo.cont_file.cont_email, dbo.ord_cont.ord_cont_note, dbo.ord_cont.ord_cont_payee " +
                "FROM dbo.cont_file INNER JOIN " +
                "dbo.ord_cont ON dbo.cont_file.cont_serial = dbo.ord_cont.ord_cont_cont_serial " +
                "WHERE dbo.ord_cont.ord_cont_cont_serial = " +
                orderId;

            DataSet obj = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, bill_to);

            if (obj.Tables.Count > 0)
            {
                var dt = obj.Tables[0];

                if (dt.Rows.Count == 0)
                {

                }
                else
                {
                    var row = dt.Rows[0];

                    BillToInfo.Name = row["cont_name"].ToString();
                    BillToInfo.Company = row["cont_company"].ToString();
                    BillToInfo.Address1 = row["cont_addr1"].ToString();
                    BillToInfo.Address2 = row["cont_addr2"].ToString();
                    BillToInfo.Address3 = row["cont_addr3"].ToString();
                    BillToInfo.State = row["cont_state"].ToString();
                    BillToInfo.Zip = row["cont_zip"].ToString();
                    BillToInfo.County = row["cont_county"].ToString();
                    BillToInfo.Country = row["cont_country"].ToString();
                    BillToInfo.Phone1 = row["cont_phone1"].ToString();
                    BillToInfo.Phone2 = row["cont_phone2"].ToString();
                    BillToInfo.email = row["cont_email"].ToString();
                    BillToInfo.City = row["cont_city"].ToString();
                    BillToInfo.Fax = row["cont_fax"].ToString();
                    BillToInfo.Cell_phone = row["cont_cell"].ToString();
               



                }

            }
            */

           // string sold_to2 = "SELECT  * from ord_cc_portal_soldto where cc_serial_soldto=" + ser;
            string sold_to = " SELECT * from ord_file where ord_serial = " + orderId;

            DataSet obj2 = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, sold_to);

            ShipToInfo = new OrderInfo();
            SoldToInfo = new OrderInfo();
            BillToInfo = new OrderInfo();

            if (obj2.Tables.Count > 0)
            {
                var dt = obj2.Tables[0];
                if (dt.Rows.Count == 0)
                {
                    NextButton.Visible = false;
                    ValidationMessage.Visible = true;
                }
                else
                {

                 
                    var row = dt.Rows[0];

                    SoldToInfo.Name = row["ord_sold_name"].ToString();
                    SoldToInfo.Company = row["ord_sold_company"].ToString();
                    SoldToInfo.Address1 = row["ord_sold_addr1"].ToString();
                    SoldToInfo.Address2 = row["ord_sold_addr2"].ToString();
                    SoldToInfo.Address3 = row["ord_sold_addr3"].ToString();
                    SoldToInfo.City = row["ord_sold_city"].ToString();
                    SoldToInfo.State = row["ord_sold_state"].ToString();
                    SoldToInfo.Zip = row["ord_sold_zip"].ToString();
                    SoldToInfo.County = row["ord_sold_county"].ToString();
                    SoldToInfo.Country = row["ord_sold_country"].ToString();
                    SoldToInfo.Phone1 = row["ord_sold_phone1"].ToString();
                    SoldToInfo.Phone2 = row["ord_sold_phone2"].ToString();
                    SoldToInfo.email = row["ord_sold_email"].ToString();     
                    SoldToInfo.Fax = row["ord_sold_fax"].ToString();
                    SoldToInfo.Cell_phone = row["ord_sold_cell"].ToString();


                    BillToInfo.Name = row["ord_sold_name"].ToString();
                    BillToInfo.Company = row["ord_sold_company"].ToString();
                    BillToInfo.Address1 = row["ord_sold_addr1"].ToString();
                    BillToInfo.Address2 = row["ord_sold_addr2"].ToString();
                    BillToInfo.Address3 = row["ord_sold_addr3"].ToString();
                    BillToInfo.City = row["ord_sold_city"].ToString();
                    BillToInfo.State = row["ord_sold_state"].ToString();
                    BillToInfo.Zip = row["ord_sold_zip"].ToString();
                    BillToInfo.County = row["ord_sold_county"].ToString();
                    BillToInfo.Country = row["ord_sold_country"].ToString();
                    BillToInfo.Phone1 = row["ord_sold_phone1"].ToString();
                    BillToInfo.Phone2 = row["ord_sold_phone2"].ToString();
                    BillToInfo.email = row["ord_sold_email"].ToString();
                    BillToInfo.Fax = row["ord_sold_fax"].ToString();
                    BillToInfo.Cell_phone = row["ord_sold_cell"].ToString();
                }

            }

            string ship_to = "SELECT * from ord_gen where ord_gen_type = 'O' and ord_gen_ord_serial = " + orderId;

            DataSet obj = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, ship_to);

            if (obj.Tables.Count > 0)
            {
                var dt = obj.Tables[0];

                if (dt.Rows.Count == 0)
                {

                    NextButton.Visible = false;
                }
                else
                {
                    var row = dt.Rows[0];

                    ShipToInfo.Name = row["ord_gen_ship_name"].ToString();
                    ShipToInfo.Company = row["ord_gen_ship_company"].ToString();
                    ShipToInfo.Address1 = row["ord_gen_ship_addr1"].ToString();
                    ShipToInfo.Address2 = row["ord_gen_ship_addr2"].ToString();
                    ShipToInfo.Address3 = row["ord_gen_ship_addr3"].ToString();
                    ShipToInfo.State = row["ord_gen_ship_state"].ToString();
                    ShipToInfo.Zip = row["ord_gen_ship_zip"].ToString();
                    ShipToInfo.County = row["ord_gen_ship_county"].ToString();
                    ShipToInfo.Country = row["ord_gen_ship_country"].ToString();
                    ShipToInfo.Phone1 = row["ord_gen_ship_phone1"].ToString();
                    ShipToInfo.Phone2 = row["ord_gen_ship_phone2"].ToString();
                    ShipToInfo.email = row["ord_gen_ship_email"].ToString();
                    ShipToInfo.City = row["ord_gen_ship_city"].ToString();
                    ShipToInfo.Fax = row["ord_gen_ship_fax"].ToString();
                    ShipToInfo.Cell_phone = row["ord_gen_ship_cell"].ToString();




                }

            }
            Session["BillToInfo"] = BillToInfo;
            Session["SoldToInfo"] = SoldToInfo;
            Session["ShipToInfo"] = ShipToInfo;
            string sql =
                "select ord_gen_serial,ord_gen_ord_serial,ord_gen_balance from ord_gen where ord_gen_serial =" +
                orderId;

             obj2 = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.Text, sql);

            if (obj2.Tables.Count > 0)
            {
                var dt = obj2.Tables[0];
                Price = (decimal)dt.Rows[0]["ord_gen_balance"];
                Session["Price"] = Price;
            }

        }
        public decimal Price = 0;

        private void PopulateTextBoxes()
        {
            S_Name.Text = SoldToInfo.Name;
            S_Country.Text = SoldToInfo.Country;
            S_County.Text = SoldToInfo.County;
            S_State.Text = SoldToInfo.State;
            S_Address_1.Text = SoldToInfo.Address1;
            S_Address_2.Text = SoldToInfo.Address2;
            S_Address_3.Text = SoldToInfo.Address3;
            S_City.Text = SoldToInfo.City;
            S_Company.Text = SoldToInfo.Company;
            S_EMail.Text = SoldToInfo.email;
            S_Zip.Text = SoldToInfo.Zip;
            S_Phone_1.Text = SoldToInfo.Phone1;
            S_Phone_2.Text = SoldToInfo.Phone2;
            S_Phone_cell.Text = SoldToInfo.Cell_phone;
            S_Fax.Text = SoldToInfo.Fax;



            B_Name.Text = BillToInfo.Name;
            B_Country.Text = BillToInfo.Country;
            B_County.Text = BillToInfo.County;
            B_State.Text = BillToInfo.State;
            B_Address_1.Text = BillToInfo.Address1;
            B_Address_2.Text = BillToInfo.Address2;
            B_Address_3.Text = BillToInfo.Address3;
            B_City.Text = BillToInfo.City;
            B_Company.Text = BillToInfo.Company;
            B_Email.Text = BillToInfo.email;
            B_Zip.Text = BillToInfo.Zip;
            B_Phone_1.Text = BillToInfo.Phone1;
            B_Phone_2.Text = BillToInfo.Phone2;
            B_Phone_cell.Text = BillToInfo.Cell_phone;
            B_Fax.Text = BillToInfo.Fax;


            Sp_Name.Text = ShipToInfo.Name;
            Sp_Country.Text = ShipToInfo.Country;
            Sp_County.Text = ShipToInfo.County;
            Sp_State.Text = ShipToInfo.State;
            Sp_Address_1.Text = ShipToInfo.Address1;
            Sp_Address_2.Text = ShipToInfo.Address2;
            Sp_Address_3.Text = ShipToInfo.Address3;
            Sp_City.Text = ShipToInfo.City;
            Sp_Company.Text = ShipToInfo.Company;
            Sp_Email.Text = ShipToInfo.email;
            Sp_Zip.Text = ShipToInfo.Zip;
            Sp_Phone_1.Text = ShipToInfo.Phone1;
            Sp_Phone_2.Text = ShipToInfo.Phone2;
            Sp_Phone_cell.Text = ShipToInfo.Cell_phone;
            Sp_Fax.Text = ShipToInfo.Fax;

        }

        private void UpdateOrderDtos()
        {
            if (BillToInfo == null)
            {

                ShipToInfo = new OrderInfo();
                SoldToInfo = new OrderInfo();
                BillToInfo = new OrderInfo();

            }
            SoldToInfo.Name = S_Name.Text;
            SoldToInfo.Country = S_Country.Text;

            SoldToInfo.County = S_County.Text;
            SoldToInfo.State = S_State.Text;
            SoldToInfo.Address1 = S_Address_1.Text;
            SoldToInfo.Address2 = S_Address_2.Text;
            SoldToInfo.Address3 = S_Address_3.Text;

            SoldToInfo.City = S_City.Text;

            SoldToInfo.Company = S_Company.Text;
            SoldToInfo.email = S_EMail.Text;
            SoldToInfo.Zip = S_Zip.Text;
            SoldToInfo.Phone1 = S_Phone_1.Text;
            SoldToInfo.Phone2 = S_Phone_2.Text;
            SoldToInfo.Cell_phone = S_Phone_cell.Text;
            SoldToInfo.Fax = S_Fax.Text;



            BillToInfo.Name = B_Name.Text;
            BillToInfo.Country = B_Country.Text;
            BillToInfo.County = B_County.Text;
            BillToInfo.State = B_State.Text;
            BillToInfo.Address1 = B_Address_1.Text;
            BillToInfo.Address2 = B_Address_2.Text;
            BillToInfo.Address3 = B_Address_3.Text;
            BillToInfo.City = B_City.Text;
            BillToInfo.Company = B_Company.Text;
            BillToInfo.email = B_Email.Text;
            BillToInfo.Zip = B_Zip.Text;
            BillToInfo.Phone1 = B_Phone_1.Text;
            BillToInfo.Phone2 = B_Phone_2.Text;
            BillToInfo.Cell_phone = B_Phone_cell.Text;
            BillToInfo.Fax = B_Fax.Text;

            ShipToInfo.Name = Sp_Name.Text;
            ShipToInfo.Country = Sp_Country.Text;
            ShipToInfo.County = Sp_County.Text;
            ShipToInfo.State = Sp_State.Text;
            ShipToInfo.Address1 = Sp_Address_1.Text;
            ShipToInfo.Address2 = Sp_Address_2.Text;
            ShipToInfo.Address3 = Sp_Address_3.Text;
            ShipToInfo.City = Sp_City.Text;
            ShipToInfo.Company = Sp_Company.Text;
            ShipToInfo.email = Sp_Email.Text;
            ShipToInfo.Zip = Sp_Zip.Text;
            ShipToInfo.Phone1 = Sp_Phone_1.Text;
            ShipToInfo.Phone2 = Sp_Phone_2.Text;
            ShipToInfo.Cell_phone = Sp_Phone_cell.Text;
            ShipToInfo.Fax = Sp_Fax.Text;

        }


        private void SubmitInfoToDb()
        {

            

        }



       
        protected void PrevButton_OnServerClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Preview.aspx");
        }
        protected void SubmitButton_OnServerClick(object sender, EventArgs e)
        {
            try
            {
                UpdateOrderDtos();
                SubmitInfoToDb();

                Session["BillToInfo"] = BillToInfo;
                Session["SoldToInfo"] = SoldToInfo;
                Session["ShipToInfo"] = ShipToInfo;

                Response.Redirect("Step2.aspx");
            }
            catch (Exception e2)
            {
                logger.Error(e.ToString);
            }
        }

        private void CopyInfo(OrderInfo from, OrderInfo to)
        {
            to = from.copy();
        }

        protected void CopyOptionChanged(object sender, EventArgs e)
        {

            DropDownList l = sender as DropDownList;
            if(l == null) return;

            if (l.ID.Equals("SoldToCopy"))
            {
                if (l.SelectedIndex == 1)
                    SoldToInfo = BillToInfo.copy(); 
                else if (l.SelectedIndex == 2)
                    SoldToInfo = ShipToInfo.copy();// CopyInfo(ShipToInfo, SoldToInfo);
                
            }

            if (l.ID.Equals("BillToCopy"))
            {
                if (l.SelectedIndex == 1)
                    BillToInfo = ShipToInfo.copy();
               
                else if (l.SelectedIndex == 2)
                    BillToInfo = SoldToInfo.copy();
                
            }

            if (l.ID.Equals("ShipToCopy"))
            {
                if (l.SelectedIndex == 1)
                    ShipToInfo = BillToInfo.copy();
                //CopyInfo(BillToInfo, ShipToInfo);
                else if (l.SelectedIndex == 2)
                    ShipToInfo = SoldToInfo.copy();
                //CopyInfo(SoldToInfo, ShipToInfo);
            }
            Session["BillToInfo"] = BillToInfo;
            Session["SoldToInfo"] = SoldToInfo;
            Session["ShipToInfo"] = ShipToInfo;
           // Session["copy_redirect"] = Boolean.TrueString;
            // UpdateOrderDtos();
            // PopulateTextBoxes();
            Response.Redirect("Step1.aspx");
        }

     
    }
}