﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WineCellarPayment.Model {
    public class WineOrder {
        public WineOrder(string slug) {
           
            this.OrderID = Guid.NewGuid();

        }
        public string ProductName { get; set; }
        public decimal Price { get; set; }



        public string AuthCode { get; set; }
        public string TransactionID { get; set; }
        public string OrderMessage { get; set; }
        public Guid OrderID { get; set; }
    }
}
