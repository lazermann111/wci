﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WineCellarPayment.Model
{
    public class PaymentPayload
    {
       /* "payload":{"responseCode":1,"authCode":"4KNMVB","avsResponse":"Y","authAmount":20.00,"entityName":"transaction","id":"60100270358"}}*/

        public string responseCode;
        public string authCode;
        public string avsResponse;
        public string authAmount;
        public string entityName;
        public string id;

        public override string ToString()
        {
            return $"{nameof(responseCode)}: {responseCode}, {nameof(authCode)}: {authCode}, {nameof(avsResponse)}: {avsResponse}, {nameof(authAmount)}: {authAmount}, {nameof(entityName)}: {entityName}, {nameof(id)}: {id}";
        }
    }
}