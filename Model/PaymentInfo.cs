﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WineCellarPayment.Model
{
    /*{"notificationId":"ce1a636e-2e3c-435f-b14f-9e1dbd32e5d1",
    "eventType":"net.authorize.payment.authcapture.created","eventDate":"2018-03-14T11:11:06.9124528Z",
    "webhookId":"2866e74a-4e3c-4430-b9d4-650af34b4b29",
    "payload":{"responseCode":1,"authCode":"4KNMVB","avsResponse":"Y","authAmount":20.00,"entityName":"transaction","id":"60100270358"}}*/
    public class PaymentInfo
    {
        public string notificationId;
        public string eventType;
        public string webhookId;
        public PaymentPayload Payload;


        public override string ToString()
        {
            return $"{nameof(notificationId)}: {notificationId}, {nameof(eventType)}: {eventType}, {nameof(webhookId)}: {webhookId}, {nameof(Payload)}: {Payload}";
        }
    }

   
}