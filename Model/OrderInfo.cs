﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WineCellarPayment.Model
{
    public class OrderInfo
    {
        public string Name;
        public string Company;
        public string Address1;
        public string Address2;
        public string Address3;
        public string City;
        public string State;
        public string Zip;
        public string County;
        public string Country;
        public string Phone1;
        public string Phone2;
        public string Fax;
        public string Cell_phone;
        public string email;

        public OrderInfo()
        {
        }

        public OrderInfo(string name, string company, string address1, string address2, string address3, string city, string state, string zip, string county, string country, string phone1, string phone2, string fax, string cellPhone, string email)
        {
            Name = name;
            Company = company;
            Address1 = address1;
            Address2 = address2;
            Address3 = address3;
            City = city;
            State = state;
            Zip = zip;
            County = county;
            Country = country;
            Phone1 = phone1;
            Phone2 = phone2;
            Fax = fax;
            Cell_phone = cellPhone;
            this.email = email;
        }

        public OrderInfo copy()
        {
            return new OrderInfo(Name,Company,Address1,Address2,Address3,City,State,Zip,County,Country,Phone1,Phone2,Fax,Cell_phone,email);
        }
    }
}